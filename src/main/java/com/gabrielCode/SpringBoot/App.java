package com.gabrielCode.SpringBoot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gabrielCode.beans.Mundo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("com/gabrielCode/xml/beans.xml");
    	Mundo m = (Mundo) appContext.getBean("mundo");
    	System.out.println(m.getSaludo());
    }
}
